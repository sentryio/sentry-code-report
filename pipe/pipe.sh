#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

source "$(dirname "$0")/common.sh"

enable_debug
extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
fi

info "Executing the pipe..."

# Required parameters
SENTRY_PROJECT_ID=${SENTRY_PROJECT_ID:?'SENTRY_PROJECT_ID variable missing.'}
SENTRY_ORG=${SENTRY_ORG:?'SENTRY_ORG variable missing.'}

export SENTRY_PROJECT=$SENTRY_PROJECT_ID
export SENTRY_ORG=$SENTRY_ORG

python3 /usr/bin/insights.py