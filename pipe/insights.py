import os
import requests
import json
from urllib.parse import quote_plus

class CodeInsights(object):
    def __init__(self):
        self.workspace = os.environ.get("BITBUCKET_WORKSPACE")
        self.reposlug = os.environ.get("BITBUCKET_REPO_SLUG")
        self.commit = os.environ.get("BITBUCKET_COMMIT")
        self.pull_request_id = os.environ.get("BITBUCKET_PR_ID")
        self.sentry_org = os.environ.get("SENTRY_ORG")
        self.sentry_project_id = os.environ.get("SENTRY_PROJECT_ID")

        self.proxies = {
            "http": "http://host.docker.internal:29418",
        }

    def fetch_diff(self):
        if self.pull_request_id:
            pr_diff_url = u"https://api.bitbucket.org/2.0/repositories/{}/{}/pullrequests/{}/diffstat".format(
                self.workspace, self.reposlug, self.pull_request_id
            )
        else:
            pr_diff_url = u"https://api.bitbucket.org/2.0/repositories/{}/{}/diffstat/{}".format(
                self.workspace, self.reposlug, self.commit
            )
        
        results = requests.get(pr_diff_url, proxies=self.proxies)

        if results:
            return results.json()
        
        else:
            raise Exception('Bitbucket request failed with status: {}'.format(results.status_code), results)
                
    def generate_report(self):
        try:  
            results = self.fetch_diff()

            if results == None:
                return 
            
            diff_results = results["values"]

            if len(diff_results) == 0:
                print("No changes!")
                return

            data = {
                "details": "Check the files modified in this pull request for potential unresolved issues in Sentry.",
                "title": "Sentry Errors",
                "external_id": 1,
                "report_type": "COVERAGE",
            }

            headers = {"Content-Type": "application/json"}

            url = u"https://api.bitbucket.org/2.0/repositories/{}/{}/commit/{}/reports/1".format(
                self.workspace, self.reposlug, self.commit
            )

            report = requests.put(url, data=json.dumps(data), headers=headers, proxies=self.proxies)

            for index, diff in enumerate(diff_results, start=2):
                if not diff["new"]:
                    continue

                file_path = diff["new"]["path"]
                annotation_data = {
                    "annotation_type": "BUG",
                    "external_id": index,
                    "summary": "Check Sentry for unresolved issues",
                    "path": file_path,
                    "link": u"https://sentry.io/organizations/{}/issues/?project={}&query=is%3Aunresolved+{}&statsPeriod=90d".format(
                        self.sentry_org, self.sentry_project_id, quote_plus(file_path)
                    ),
                    "line": 0,
                }
                annotation_url = u"https://api.bitbucket.org/2.0/repositories/{}/{}/commit/{}/reports/1/annotations/{}".format(
                    self.workspace, self.reposlug, self.commit, index
                )

                annotation = requests.put(
                    annotation_url,
                    data=json.dumps(annotation_data),
                    headers=headers,
                    proxies=self.proxies,
                )
        except Exception as e: 
            print("Something went wrong!\n")
            print("BITBUCKET_PR_ID:", self.pull_request_id,"\n")
            print("BITBUCKET_COMMIT", self.commit, "\n")
            print("BITBUCKET_WORKSPACE", self.workspace, "\n")
            print("BITBUCKET_REPO_SLUG", self.reposlug, "\n")
            print(e)
            raise


if __name__ == '__main__':
    CodeInsights().generate_report()