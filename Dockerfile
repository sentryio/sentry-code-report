FROM python:3.7-slim

RUN pip install --no-cache-dir requests

COPY pipe /usr/bin/

RUN chmod a+x /usr/bin/*.sh

ENTRYPOINT ["/usr/bin/pipe.sh"]
