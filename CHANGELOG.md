# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.6

- patch: Add try-except

## 0.0.5

- patch: Update Dockerfile to use Debian-based Python image

## 0.0.4

- patch: update pipe.yml

## 0.0.3

- patch: update pipe.yml

## 0.0.2

- patch: update readme & rm todo

## 0.0.1

- patch: initial

